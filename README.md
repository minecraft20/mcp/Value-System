# This is a fork from Tojatta's system. All credit goes to him. Forked by MemesValkyrie.

# Side note, because this saves in json, you will need gson as a dependancy. MCP has it by default.

# Value System

This is a annotated value system I wrote. I just felt like making something for people to use for some reason because I got bored. Please enjoy!

## Getting Started

How to get this set-up and implemented.

### Download

[Latest](https://mega.nz/#!cxZXmZoB!5fVRsnWdizIwVdjwYrA_YQQHUE5V_dCBPIfgivFRy0Q)

### Installing

With the downloaded library, add it to your class path. Simple enough? 

### Example

You have to make an instance of the value manager. How ever you really wanna do it...

```
public ValueManager valueManager = new ValueManager();
```

Now you have to register your classes so it can load all the values from them.

An example you can place that in your module super class and it will register every value in every module for you.

```
valueManager.register(this);
```

Examples of values

Boolean:
```
@BooleanValue(label = "Example Boolean Value")
private boolean exBoolean = true;
```

String:
```
@StringValue(label = "Example String Value")
private String exString = "Tojatta";
```

Number:
```
@NumberValue(label = "Example Int Value")
private int intNumber = 1;

@NumberValue(label = "Example Capped Int Value", minimum = "1", maximum = "500")
private int cappedIntNumber = 45;

@NumberValue(label = "Example Double Value")
private double doubleNumber = 7D;

@NumberValue(label = "Example Capped Double Value", minimum = "0.2", maximum = "20.5")
private double cappedDoubleNumber = 6.25D;
```

You can grab values by name!
```
valueManager.getOptionalValueName("Example String Value").get().getValue(); //Returns "Tojatta"
```

You can get all values from certain classes!
```
valueManager.getValuesFromClass(killauraModule); //Returns a list of all the values in the class 'killaura'
```

## Thanks
N3xuz - Giving me some feedback and info on parsing values.

Ddong - Giving me some feedback.
