package me.valkyrie.value;

import me.tojatta.api.value.ValueManager;
import me.tojatta.api.value.impl.annotations.NumberValue;
import me.tojatta.api.value.impl.annotations.StringValue;

import java.io.File;

/**
 * Created by Zeb on 7/13/2016.
 */
public class Example {

    private static final ValueManager valueManager = new ValueManager();
    private static ValueFileHandler fileHandler = null;

    /*
        These are the example values from tojatta's tutorial
     */

    @StringValue(label = "Example String Value")
    private String exString = "Tojatta";

    @NumberValue(label = "Example Int Value")
    private int intNumber = 1;

    @NumberValue(label = "Example Capped Int Value", minimum = "1", maximum = "500")
    private int cappedIntNumber = 45;

    @NumberValue(label = "Example Double Value")
    private double doubleNumber = 7D;

    @NumberValue(label = "Example Capped Double Value", minimum = "0.2", maximum = "20.5")
    private double cappedDoubleNumber = 6.25D;

    public Example() {
        valueManager.register(this);
        fileHandler.save();
        intNumber = 10;
        fileHandler.load(); //Value is back to 1;
    }

    public static void main(String[] args) {
        fileHandler = new ValueFileHandler(valueManager, new File("<file>.json"));
        new Example();
    }

}
