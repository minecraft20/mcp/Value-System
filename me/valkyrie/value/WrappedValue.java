package me.valkyrie.value;

import me.tojatta.api.value.Value;

/**
 * Created by Zeb on 7/13/2016.
 */
public class WrappedValue {

    private String label, parent, type;

    private Object value;

    public WrappedValue(Value value) {
        this.label = value.getLabel();
        this.parent = value.getObject().getClass().getName();
        this.type = value.getValue().getClass().getSimpleName().toLowerCase();
        this.value = value.getValue();
    }

    public String getLabel() {
        return label;
    }

    public String getParent() {
        return parent;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }
}
