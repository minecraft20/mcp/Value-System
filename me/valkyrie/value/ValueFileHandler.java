package me.valkyrie.value;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import me.tojatta.api.value.Value;
import me.tojatta.api.value.ValueManager;

import javax.annotation.processing.FilerException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Zeb on 7/13/2016.
 */
public class ValueFileHandler {

    private ValueManager valueManager;
    private File file;

    public static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public ValueFileHandler(ValueManager valueManager, File file) {
        this.valueManager = valueManager;
        this.file = file;

        if (file.isDirectory()) return;

        if (file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Attempts to save the values to the file.
     *
     * @return success
     */
    public boolean save() {
        if (file == null || file.isDirectory()) return false;

        List<WrappedValue> wrappedValues = new ArrayList();

        valueManager.getValues().keySet().stream().forEach(value -> {
            wrappedValues.add(new WrappedValue(value));
        });

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(file));

            String json = gson.toJson(wrappedValues);

            writer.println(json);

            writer.close();
            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Attempts to load the values from the file.
     *
     * @return success
     */
    public boolean load() {
        if (file == null || file.isDirectory()) return false;

        try {

            Object input = gson.fromJson(new FileReader(file), Object.class);

            List list = (List) input;

            for (Object object : list) {
                LinkedTreeMap linkedTreeMap = (LinkedTreeMap) object;

                WrappedValue wrappedValue = gson.fromJson(gson.toJson(linkedTreeMap), WrappedValue.class);

                for (Value value : valueManager.getValues().keySet()) {
                    if (value.getObject().getClass().getName().equals(wrappedValue.getParent())
                            && value.getLabel().equalsIgnoreCase(wrappedValue.getLabel())) {

                        Class vClass = value.getValue().getClass();

                        if (wrappedValue.getValue().getClass() == Double.class) {
                            value.setValue(getAbsoluteNumberValue((Double) wrappedValue.getValue(), wrappedValue.getType()));
                        } else {
                            value.setValue(vClass.cast(wrappedValue.getValue()));
                        }
                    }
                }
            }

            return true;
        } catch (Exception exception) {
            exception.printStackTrace();
            return false;
        }
    }

    /**
     * Find the absolute value of a given double.
     *
     * @param object value that needs to be determined.
     * @param type class name of the value that is used in parsing.
     * @return
     */
    private Object getAbsoluteNumberValue(Double object, String type) {
        switch (type) {
            case "integer":
                return new Integer(object.intValue());
            case "float":
                return new Float(object.floatValue());
            case "double":
                return object;
            default:
                return object;
        }
    }


}
