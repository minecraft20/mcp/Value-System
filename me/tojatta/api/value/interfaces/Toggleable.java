package me.tojatta.api.value.interfaces;

/**
 * Created by Tojatta on 7/12/2016.
 */
public interface Toggleable {

    boolean isEnabled();

    void setEnabled(boolean enabled);

    void onEnable();

    void onDisable();

    void onToggle();

    void toggle();

}
