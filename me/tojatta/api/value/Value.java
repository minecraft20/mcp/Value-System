package me.tojatta.api.value;

import me.tojatta.api.value.interfaces.Labelable;

import java.lang.reflect.Field;

/**
 * Created by Tojatta on 7/12/2016.
 */
public class Value<T> implements Labelable {

    protected String label;
    protected Object object;
    protected Field field;

    public Value(String label, Object object, Field field) {
        this.label = label;
        this.object = object;
        this.field = field;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public T getValue() {
        try {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            T val = (T) field.get(object);
            field.setAccessible(accessible);
            return val;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setValue(T value) {
        try {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            field.set(object, value);
            field.setAccessible(accessible);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    public Object getObject() {
        return object;
    }
}
